const { BlobServiceClient } = require('@azure/storage-blob');
const uuidv1 = require('uuid/v1');
const AZURE_STORAGE_CONNECTION_STRING = process.env.AZURE_STORAGE_CONNECTION_STRING;


async function main() {
  console.log('Azure Blob storage v12 - JavaScript quickstart sample');
  // Quick start code goes here
  // Create the BlobServiceClient object which will be used to create a container client
  const blobServiceClient = await BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);

  // Create a unique name for the container
  const containerName = 'demo';

  console.log('\nCreating container...');
  console.log('\t', containerName);

  // Get a reference to a container
  const containerClient = await blobServiceClient.getContainerClient(containerName);



  // Create a unique name for the blob
  const blobName = 'quickstart' + uuidv1() + '.txt';

  // Get a block blob client
  const blockBlobClient = containerClient.getBlockBlobClient(blobName);

  console.log('\nUploading to Azure storage as blob:\n\t', blobName);
  // Upload data to the blob
  const data = 'Hello, World Idan Lazar!';
  const uploadBlobResponse = await blockBlobClient.upload(data, data.length);
  console.log("Blob was uploaded successfully. requestId: ", uploadBlobResponse.requestId);
}

main().then(() => console.log('Done')).catch((ex) => console.log(ex.message));